Shader "Turnyworld/CombinedShader" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		[KeywordEnum(Rim,Reflect,RimPerPixel)] _Lighting ("Lighting", Float) = 0
		_RimColor ("Rim Color", Vector) = (1,1,1,1)
		_RimPower ("Rim Power", Range(0.1, 8)) = 3
		[Space(15)] [KeywordEnum(Off,On)] _Fog ("Fog Enabled", Float) = 0
		[Space(15)] [KeywordEnum(Off,On)] _Reflect ("Reflection Enabled", Float) = 0
		_Cube ("Cubemap", Cube) = "" {}
		_DiffuseStrength ("Reflection Power", Range(0, 1)) = 0.1
		_ReflectionStrength ("Reflection Power2", Range(0, 1)) = 0.1
		_MinLight ("Min Light", Range(0, 1)) = 0
		_MaxLight ("Max Light", Range(0, 1)) = 1
		[Space(15)] [KeywordEnum(Enabled,Disabled)] _CUSTOM_EFFECTS ("Custom Character Effects", Float) = 0
	}
	//DummyShaderTextExporter
	SubShader{
		Tags { "RenderType"="Opaque" }
		LOD 200
		CGPROGRAM
#pragma surface surf Standard
#pragma target 3.0

		sampler2D _MainTex;
		struct Input
		{
			float2 uv_MainTex;
		};

		void surf(Input IN, inout SurfaceOutputStandard o)
		{
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	}
	Fallback "Diffuse"
}