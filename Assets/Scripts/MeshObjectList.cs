using System;
using UnityEngine;

[Serializable]
public class MeshObjectList
{
	public bool doCombine = true;

	public GameObject mainObject;
}
