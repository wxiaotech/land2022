namespace LandSliders
{
	public static class GPGSIds
	{
		public const string achievement_many_wish_to_ascend = "CgkItt3YhoUPEAIQCQ";

		public const string achievement_some_are_happy = "CgkItt3YhoUPEAIQCw";

		public const string achievement_for_glory = "CgkItt3YhoUPEAIQCA";

		public const string package_name = "com.prettygreat.landsliders";

		public const string achievement_why_destiny_manifests = "CgkItt3YhoUPEAIQBQ";

		public const string achievement_a_land_of_surprises = "CgkItt3YhoUPEAIQDg";

		public const string achievement_others_will_stay_nurturing = "CgkItt3YhoUPEAIQDQ";

		public const string achievement_with_only_power = "CgkItt3YhoUPEAIQDA";

		public const string achievement_in_the_hearts_and_butts = "CgkItt3YhoUPEAIQBg";

		public const string achievement_as_a_legend = "CgkItt3YhoUPEAIQCg";

		public const string achievement_you_decide_your_fate = "CgkItt3YhoUPEAIQEA";

		public const string achievement_keeps_the_world_turning = "CgkItt3YhoUPEAIQBA";

		public const string achievement_peaceloving_pacifist_or = "CgkItt3YhoUPEAIQAg";

		public const string achievement_becoming_a_deity_a = "CgkItt3YhoUPEAIQAQ";

		public const string achievement_of_those_who_rush = "CgkItt3YhoUPEAIQBw";

		public const string leaderboard_total_score = "CgkItt3YhoUPEAIQEw";

		public const string achievement_what_matters_is = "CgkItt3YhoUPEAIQDw";

		public const string achievement_a_hot_new_tech_startup = "CgkItt3YhoUPEAIQAw";

		public const string achievement_walk_your_own_path = "CgkItt3YhoUPEAIQEQ";

		public const string leaderboard_best_score = "CgkItt3YhoUPEAIQEg";
	}
}
