public enum HexType
{
	Empty = 0,
	Hex = 1,
	Half = 2,
	Quarter = 3,
	Crescent = 4,
	CurvedHalf = 5,
	Island = 6,
	ThreeQuater = 7,
	Any = 8
}
