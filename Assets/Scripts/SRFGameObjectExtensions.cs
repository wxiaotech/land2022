using System;
using System.Collections;
using UnityEngine;

public static class SRFGameObjectExtensions
{
	public static T GetIComponent<T>(this GameObject t) where T : class
	{
		return t.GetComponent(typeof(T)) as T;
	}

	public static T GetComponentOrAdd<T>(this GameObject obj) where T : Component
	{
		T val = obj.GetComponent<T>();
		if ((UnityEngine.Object)val == (UnityEngine.Object)null)
		{
			val = obj.AddComponent<T>();
		}
		return val;
	}

	public static void RemoveComponentIfExists<T>(this GameObject obj) where T : Component
	{
		T component = obj.GetComponent<T>();
		if ((UnityEngine.Object)component != (UnityEngine.Object)null)
		{
			UnityEngine.Object.Destroy(component);
		}
	}

	public static void RemoveComponentsIfExists<T>(this GameObject obj) where T : Component
	{
		T[] components = obj.GetComponents<T>();
		for (int i = 0; i < components.Length; i++)
		{
			UnityEngine.Object.Destroy(components[i]);
		}
	}

	public static bool EnableComponentIfExists<T>(this GameObject obj, bool enable = true) where T : MonoBehaviour
	{
		T component = obj.GetComponent<T>();
		if ((UnityEngine.Object)component == (UnityEngine.Object)null)
		{
			return false;
		}
		component.enabled = enable;
		return true;
	}

	public static void SetLayerRecursive(this GameObject o, int layer)
	{
		SetLayerInternal(o.transform, layer);
	}

	private static void SetLayerInternal(Transform t, int layer)
	{
		t.gameObject.layer = layer;
		IEnumerator enumerator = t.GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				Transform t2 = (Transform)enumerator.Current;
				SetLayerInternal(t2, layer);
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = enumerator as IDisposable) != null)
			{
				disposable.Dispose();
			}
		}
	}
}
