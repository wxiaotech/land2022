public enum HexTypeExtended
{
	None = 0,
	Water = 1,
	SetPiece = 2,
	DynamicTile = 3,
	Any = 4
}
