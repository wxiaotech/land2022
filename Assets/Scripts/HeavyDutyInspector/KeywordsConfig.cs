using System.Collections.Generic;
using UnityEngine;

namespace HeavyDutyInspector
{
	public class KeywordsConfig : ScriptableObject
	{
		public List<KeywordCategory> keyWordCategories = new List<KeywordCategory>();
	}
}
