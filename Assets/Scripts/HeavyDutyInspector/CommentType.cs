namespace HeavyDutyInspector
{
	public enum CommentType
	{
		Error = 0,
		Info = 1,
		None = 2,
		Warning = 3
	}
}
