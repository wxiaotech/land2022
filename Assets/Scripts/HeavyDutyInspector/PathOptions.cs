namespace HeavyDutyInspector
{
	public enum PathOptions
	{
		RelativeToAssets = 0,
		RelativeToResources = 1,
		FilenameOnly = 2
	}
}
