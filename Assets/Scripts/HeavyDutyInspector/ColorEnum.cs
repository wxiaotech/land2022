namespace HeavyDutyInspector
{
	public enum ColorEnum
	{
		Black = 0,
		Blue = 1,
		Cyan = 2,
		Gray = 3,
		Green = 4,
		Grey = 5,
		Magenta = 6,
		Red = 7,
		White = 8,
		Yellow = 9
	}
}
