using HeavyDutyInspector;
using UnityEngine;

public class ExampleSerializationWrappers : MonoBehaviour
{
	public int intMinValue = int.MinValue;

	public int intMaxValue = int.MaxValue;

	public short shortExample;

	public long longExample;

	public ushort ushortExample;

	public uint uintExample;

	public ulong ulongExample;

	public Int16S serializableShortExample;

	public Int64S serializableLongExample;

	public UInt16S serializableUshortExample;

	public UInt32S serializableUintExample;

	public UInt64S serializableUlongExample;
}
