public enum VegetableType
{
	Bulb = 0,
	Grain = 1,
	Fruit = 2,
	Plant = 3,
	Squash = 4
}
